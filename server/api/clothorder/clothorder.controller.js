'use strict';

var _ = require('lodash');
var Clothorder = require('./clothorder.model');
var Cloth = require('../cloth/cloth.model');

// Get list of clothorders
exports.index = function(req, res) {
    Clothorder.find(function(err, clothorders) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(clothorders);
    });
};

// Get a single clothorder
exports.show = function(req, res) {
    Clothorder.findById(req.params.id, function(err, clothorder) {
        if (err) {
            return handleError(res, err);
        }
        if (!clothorder) {
            return res.status(404).send('Not Found');
        }
        return res.json(clothorder);
    });
};

// Creates a new clothorder in the DB.
exports.create = function(req, res) {
    var clothOrder;
    Clothorder.create(req.body, function(err, clothorder) {
        if (err) {
            console.log(err);
            return handleError(res, err);
        }
        clothOrder = clothorder;
        clothorder.clothes.forEach(updateLength, clothorder);

        return res.status(201).json(clothorder);
    });


    function updateLength(orderItem) {
        //var _thiz = this;
        Cloth.findOne({
                '_id': orderItem.cloth
            },
            function(err, cloth) {
                if (err) {
                    return handleError(res, err);
                }
                cloth.subtractLength(orderItem.length, clothOrder._id);
                cloth.save();
            });
    }
};

// Updates an existing clothorder in the DB.
exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Clothorder.findById(req.params.id, function(err, clothorder) {
        if (err) {
            return handleError(res, err);
        }
        if (!clothorder) {
            return res.status(404).send('Not Found');
        }
        var updated = _.merge(clothorder, req.body);
        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(clothorder);
        });
    });
};

// Deletes a clothorder from the DB.
exports.destroy = function(req, res) {
    Clothorder.findById(req.params.id, function(err, clothorder) {
        if (err) {
            return handleError(res, err);
        }
        if (!clothorder) {
            return res.status(404).send('Not Found');
        }
        clothorder.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};

function handleError(res, err) {
    return res.status(500).send(err);
}
