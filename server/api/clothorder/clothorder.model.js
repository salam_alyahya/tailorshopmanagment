'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/environment'),
    autoIncrement = require('mongoose-auto-increment');

var ClothorderSchema = new Schema({
  date: Date,
  price: Number,
  paid: Number,
  balance: Number,
  refNo: Number, // reference number
  user: { type : mongoose.Schema.ObjectId, ref : 'User'},
  clothes:[{
  	cloth: { type : Number, ref : 'Cloth' },
  	length: Number,
  	unit: {
  		type: String,
  		default: config.DEFAULT_UNIT
  	}
  }]
});

ClothorderSchema.plugin(autoIncrement.plugin, 'Clothorder');
// ClothorderSchema.pre('save', function(next) {
//     var thiz = this;
//     this.clothes.populate();
//     console.log(this);
//   });

module.exports = mongoose.model('Clothorder', ClothorderSchema);
