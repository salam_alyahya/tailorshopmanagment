'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');


var ClothSchema = new Schema({
    name: String, // name of the cloth type *
    type: String, // type of the cloth *
    width: Number, // general roll's width
    unitLength: Number, // length of one roll
    make: String, // place of make **
    unit: String, // unit of measurment 
    rollsCount: Number, // number of rolls *
    totalLength: Number, // the total length *
    unitSellPrice: Number, // ** price of selling a unit (SAR/ unit)
    color: String,
    dealer: { type: Number, ref: 'Dealer' }, //*
    rollsPurchase: [ //**
        {
            purchaseCount: Number, // number of rolls in this purchase
            unitLength: { type: Number, default: 0 },
            availableCount: Number, // unused number of rolls
            isActive: { type: Boolean, default: true },
            dateOfPurchase: Date, // ***
            price: Number, // *** price of purchaseCount number of rolls
        }
    ],
    clothOrders: [{ type: Number, ref: 'Clothorder' }]
});

ClothSchema.plugin(autoIncrement.plugin, {
    model: 'Cloth',
    startAt: 1
});

/**
 * Methods
 */
ClothSchema.methods = {
    subtractLength: function(length, clothOrderID) {
        this.totalLength = this.totalLength - length;
        this.rollsCount = this.totalLength / this.unitLength;
        this.clothOrders.push(clothOrderID);
    }
}


module.exports = mongoose.model('Cloth', ClothSchema);
