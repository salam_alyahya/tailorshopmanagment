'use strict';

var _ = require('lodash');
var Cloth = require('./cloth.model');

// Get list of cloths
exports.index = function(req, res) {
    Cloth.find(function(err, cloths) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(cloths);
    });
};

// Get a single cloth
exports.show = function(req, res) {
    Cloth.findById(req.params.id, function(err, cloth) {
        if (err) {
            return handleError(res, err);
        }
        if (!cloth) {
            return res.status(404).send('Not Found');
        }
        return res.json(cloth);
    });
};

// Creates a new cloth in the DB.
exports.create = function(req, res) {
    Cloth.create(req.body, function(err, cloth) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(201).json(cloth);
    });
};

// Updates an existing cloth in the DB.
exports.update = update;

function oldUpdate(req, res) {
    if (req.body._id) { delete req.body._id; }
    Cloth.findById(req.params.id, function(err, cloth) {
        if (err) {
            return handleError(res, err);
        }
        if (!cloth) {
            return res.status(404).send('Not Found');
        }
        var rollsPurchase = req.body.rollsPurchase
            .filter(hasNoId);
        var updated = _.merge(cloth, req.body);
        //console.log(rollsPurchase);
        //console.log(updated);
        //updated.rollsPurchase = [];

        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(200).json(cloth);
        });

        function hasNoId(rollPurch) {
            return rollPurch._id === undefined;
        }
    });
}

// Updates an existing cloth in the DB.
function update(req, res) {
    if (req.body._id) { delete req.body._id; }
    var rollsPurchase = req.body.rollsPurchase.filter(hasNoId),
        options = { safe: true, new: true },
        doc = {
            $pushAll: { rollsPurchase: rollsPurchase },
            $set: req.body
        };

    delete req.body.rollsPurchase;

    Cloth.findByIdAndUpdate(req.params.id, doc, options, success);


    function success(err, cloth) {
        if (err) {
            return handleError(res, err);
        }
        if (!cloth) {
            return res.status(404).send('Not Found');
        }
        return res.status(200).json(cloth);
    }

    function hasNoId(rollPurch) {
        return rollPurch._id === undefined;
    }
}

// Deletes a cloth from the DB.
exports.destroy = function(req, res) {
    Cloth.findById(req.params.id, function(err, cloth) {
        if (err) {
            return handleError(res, err);
        }
        if (!cloth) {
            return res.status(404).send('Not Found');
        }
        cloth.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};

function handleError(res, err) {
    console.log(err);
    return res.status(500).send(err);
}