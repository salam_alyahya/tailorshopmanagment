'use strict';

var should = require('should'),
    app = require('../../app'),
    request = require('supertest'),
    Dealer = require('./dealer.model');

var dealer = new Dealer({
    name: "test",
    telephone: "+096655653765",
    address: "String",
    notes: "String",
    contacts: [{
        name: "String",
        telephone: "String"
    }]
});

describe('GET /api/dealers', function() {

    it('should respond with JSON array', function(done) {
        request(app)
            .get('/api/dealers')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                if (err) return done(err);
                res.body.should.be.instanceof(Array);
                done();
            });
    });
});

describe('Dealer  model', function() {
    before(function(done) {
        // Clear dealers before testing
        Dealer.remove().exec().then(function() {
            done();
        });
    });

    afterEach(function(done) {
        Dealer.remove().exec().then(function() {
            done();
        });
    });

    it('should begin with no dealers', function(done) {
        Dealer.find({}, function(err, dealers) {
            dealers.should.have.length(0);
            done();
        });
    });


});