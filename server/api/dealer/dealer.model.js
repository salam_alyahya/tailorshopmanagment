'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');

var DealerSchema = new Schema({
  name: String,
  telephone: String,
  address: String,
  notes: String,
  contacts: [{
	name: String,
	telephone: String
  }],
});

DealerSchema.plugin(autoIncrement.plugin, {
  model: 'Dealer',
  startAt: 1});

module.exports = mongoose.model('Dealer', DealerSchema);
