'use strict';

var _ = require('lodash');
var Dealer = require('./dealer.model');

// Get list of dealers
exports.index = function(req, res) {
  Dealer.find(function (err, dealers) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(dealers);
  });
};

// Get a single dealer
exports.show = function(req, res) {
  Dealer.findById(req.params.id, function (err, dealer) {
    if(err) { return handleError(res, err); }
    if(!dealer) { return res.status(404).send('Not Found'); }
    return res.json(dealer);
  });
};

// Creates a new dealer in the DB.
exports.create = function(req, res) {
  Dealer.create(req.body, function(err, dealer) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(dealer);
  });
};

// Updates an existing dealer in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Dealer.findById(req.params.id, function (err, dealer) {
    if (err) { return handleError(res, err); }
    if(!dealer) { return res.status(404).send('Not Found'); }
    var updated = _.merge(dealer, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(dealer);
    });
  });
};

// Deletes a dealer from the DB.
exports.destroy = function(req, res) {
  Dealer.findById(req.params.id, function (err, dealer) {
    if(err) { return handleError(res, err); }
    if(!dealer) { return res.status(404).send('Not Found'); }
    dealer.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
