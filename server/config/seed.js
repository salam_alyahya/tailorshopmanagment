/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Cloth = require('../api/cloth/cloth.model');
var ClothOrder = require('../api/clothorder/clothorder.model');
var Dealer = require('../api/dealer/dealer.model');


User.find({}).remove(function() {
        User.create({
            provider: 'local',
            name: 'Test User',
            username: 'test',
            password: 'test'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Admin',
            username: 'admin',
            password: 'admin'
        }, function() {
            console.log('finished populating users');
        });
    });


// .remove(function() {
//     User.create({
//         provider: 'local',
//         name: 'Test User',
//         username: 'test',
//         password: 'test'
//     }, {
//         provider: 'local',
//         role: 'admin',
//         name: 'Admin',
//         username: 'admin',
//         password: 'admin'
//     }, function() {
//         console.log('finished populating users');
//     });
// });
