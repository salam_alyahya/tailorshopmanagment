   (function() {
       'use strict';

       angular.module('tailorShopMngApp')
           .config(['$stateProvider', function($stateProvider) {
               $stateProvider
                   .state('main', {
                       url: '/',
                       templateUrl: 'app/main/main.html',
                       controller: 'MainCtrl'
                   });
           }]);
   })();