(function() {
    'use strict';

    angular.module('tailorShopMngApp')
        .controller('MainCtrl', ['$scope', 'Auth', function($scope, Auth) {

            $scope.isAdmin = Auth.isAdmin();
        }]);
})();
