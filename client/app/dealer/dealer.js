(function() {
    'use strict';

    var getDealers = function(Dealer) {
        return Dealer.query();
    };
    getDealers.$inject = ['Dealer'];

    angular.module('tailorShopMngApp')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('dealer', {
                    url: '/dealer',
                    templateUrl: 'app/dealer/dealer.html',
                    controller: 'DealerCtrl',
                    resolve: {
                        'dealers': getDealers
                    },
                    authenticate: true
                });
        }]);


})();