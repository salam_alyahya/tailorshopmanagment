(function() {
    'use strict';

    angular.module('tailorShopMngApp')
        .factory('Dealer', ['$resource', function($resource) {
            // Service logic
            // ...
            var data = $resource('/api/dealers/:_id', {
                _id: '@_id'
            }, {
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        _id: '_id'
                    }
                }
            });


            // Public API here
            return data;
        }]);
})();