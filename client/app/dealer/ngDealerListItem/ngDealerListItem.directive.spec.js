'use strict';

describe('Directive: ngDealerListItem', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/dealer/ngDealerListItem/ngDealerListItem.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-dealer-list-item></ng-dealer-list-item>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngDealerListItem directive');
  }));
});
