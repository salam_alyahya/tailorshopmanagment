(function() {
    'use strict';
    var itemController = function($scope) {

    };
    itemController.$inject = ['$scope'];

    angular.module('tailorShopMngApp')
        .directive('ngDealerListItem', function() {
            return {
                templateUrl: 'app/dealer/ngDealerListItem/ngDealerListItem.html',
                restrict: 'EA',
                scope: {
                    mDealer: '=',
                    onEdit: '&',
                    onRemove: '&',
                    showEdit: '='
                },
                controller: itemController,
                link: function(scope, element, attrs) {}
            };
        });


})();