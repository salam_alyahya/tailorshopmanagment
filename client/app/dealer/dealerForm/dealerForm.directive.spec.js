'use strict';

describe('Directive: dealerForm', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/dealer/dealerForm/ngDealerForm.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-dealer-form></ng-dealer-form>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngDealerForm directive');
  }));
});
