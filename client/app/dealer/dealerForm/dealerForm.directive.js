 (function() {

     'use strict';

     var formController = function($scope, Dealer, $mdToast, $log) {

         $scope.addnewContact = function() {
             if (!$scope.ngModel.contacts) {
                 $scope.ngModel.contacts = [];
             }
             $scope.ngModel.contacts.push({});
         };

         $scope.removeContact = function(i) {
             $scope.ngModel.contacts.splice(i, 1);
         };

         $scope.saveModel = function(model) {
             if ($scope.dealerForm.$valid) {
                 if (model._id) {
                     model.$update(succes, handleError);
                 } else {
                     model.$save(succes, handleError);
                 }
             }

             function succes(res) {
                 //console.log(res);
                 resetDealer();
                 showSuccessToast();
                 $scope.onUpdate();
             }
         };

         function handleError(err) {
             $log.error(err);
             showFailToast();
         }

         function resetDealer() {
             $scope.ngModel = new Dealer({
                 contacts: []
             });
             $scope.dealerForm.$setPristine();
             $scope.dealerForm.$setUntouched();
         }

         function validateFrom() {
             angular.forEach($scope.dealerForm.$error.required, function(field) {
                 field.$setDirty();
             });
         }

         function showSuccessToast() {
             showToast('تم ادخال الموزع بنجاح');
         }

         function showFailToast() {
             showToast('اووبس، السستم تعبان وصار شي غلط');
         }

         function showToast(msg) {
             $mdToast.show(
                 $mdToast.simple()
                 .textContent(msg)
                 .position('bottom right')
                 .hideDelay(3000)
             );
         }
     };

     formController.$inject = ['$scope', 'Dealer', '$mdToast', '$log'];

     angular.module('tailorShopMngApp')
         .directive('ngDealerForm', function() {
             return {
                 templateUrl: 'app/dealer/dealerForm/ngDealerForm.html',
                 restrict: 'EA',
                 require: '^ngModel',
                 scope: {
                     ngModel: '=',
                     onSave: '&',
                     onUpdate: '&',
                     ngIsEdit: '='
                 },
                 controller: formController,
                 link: function(scope, element, attrs, ngModelCtrl) {
                     //var contactsElement = angular.element()
                     //console.log(element);
                 }
             };
         });
 })();
