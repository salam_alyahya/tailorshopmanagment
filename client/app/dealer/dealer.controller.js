(function() {

    'use strict';



    var DealerController = function($scope, Dealer, Auth, dealers, $mdToast, $log, $mdDialog) {
        $scope.message = 'Hello';
        $scope.onSave = saveDealer;
        $scope.dealers = dealers;
        $scope.editDealer = editDealer;
        $scope.isEditDealer = false;
        $scope.removeDealer = removeDealer;
        $scope.newDealer = newDealer;
        $scope.updateDealers = updateDealers;
        $scope.selectedIndex = 1;
        $scope.isAdmin = Auth.isAdmin;
        $scope.toggleSelect = toggleSelect;
        var selectedItem;
        updateDealers();


        $scope.dealer = new Dealer({
            name: '',
            contacts: [],
        });

        $scope.$watch('selectedIndex', function(current, old) {
            if (current === 1) {
                $scope.isEditDealer = false;
                $scope.dealer = new Dealer({
                    name: '',
                    contacts: [],
                });
            }
        });


        function getDealers() {
            return Dealer.query();
        }

        function saveDealer(model) {
            //console.log('model', model);
            Dealer.save(model)
                .$promise
                .then(
                    function(res) {
                        // console.log(res);
                        //console.log('saved: ', res);
                        updateDealers();
                        showSuccessToast();
                    }, handleError);
        }

        function editDealer(model, i) {
            $scope.dealer = model;
            $scope.isEditDealer = true;
            $scope.selectedIndex = 0;
        }

        function removeDealer($event, model, i) {
            //console.log('remove');
            showConfirm($event, success, handleError);

            function success() {
                Dealer.remove(model)
                    .$promise
                    .then(function() {
                        updateDealers();
                        showSuccessRemoveToast();
                    }, handleError);
            }
        }

        function handleError(err) {
            $log.error(err);
            showFailToast();
        }

        function updateDealers() {
            $scope.dealers = getDealers();
            $scope.isEditDealer = false;
            $scope.dealer = new Dealer({
                contacts: []
            });
        }

        function newDealer() {
            $scope.dealer = new Dealer();
            //console.log($scope.dealer);
            $scope.isEditDealer = true;
        }

        function toggleSelect(dealer) {
            if (angular.isUndefined(dealer.selected)) {
                dealer.selected = false;
                dealer.toggleSelect = function() {
                    dealer.selected = !dealer.selected;
                };
            }
            if (selectedItem) {
                selectedItem.toggleSelect();
                selectedItem = undefined;
            }

            if (selectedItem !== dealer) {
                dealer.toggleSelect();
                selectedItem = dealer;
            }
        }

        function showSuccessToast() {
            showToast('تم ادخال الموزع بنجاح');
        }

        function showSuccessRemoveToast() {
            showToast('تم حذف الموزع بنجاح');
        }

        function showFailToast() {
            showToast('اووبس، السستم تعبان وصار شي غلط');
        }

        function showToast(msg) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(msg)
                .position('bottom right')
                .hideDelay(3000)
            );
        }

        function showConfirm(ev, sucess, err) {
            //Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('هل انت متاكد؟')
                .textContent('سيتم حذف هذه المعلومات')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('اي احذفه له!')
                .cancel('لا لا وش لك');
            $mdDialog.show(confirm).then(sucess, err);
        }
    };

    DealerController.$inject = ['$scope', 'Dealer', 'Auth', 'dealers', '$mdToast', '$log', '$mdDialog'];

    angular.module('tailorShopMngApp')
        .controller('DealerCtrl', DealerController);
})();
