 (function() {
     'use strict';

     angular.module('tailorShopMngApp', [
             'ngCookies',
             'ngResource',
             'ngSanitize',
             'ui.router',
             'ui.bootstrap',
             'ngMaterial',
             'ngMessages',
             'ngAnimate',
             'angular-unit-converter'
         ])
         .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdThemingProvider) {
             $urlRouterProvider
                 .otherwise('/');

             $locationProvider.html5Mode(true);
             $httpProvider.interceptors.push('authInterceptor');
             // $mdThemingProvider.theme('default')
             //         .primaryPalette('blue-grey')
             //         .accentPalette('pink')
             //;
             $mdThemingProvider.theme('default')
                 .primaryPalette('blue-grey')
                 .accentPalette('grey');
         }])

     .factory('authInterceptor', ['$rootScope', '$q', '$cookieStore', '$location', function($rootScope, $q, $cookieStore, $location) {
             return {
                 // Add authorization token to headers
                 request: function(config) {
                     config.headers = config.headers || {};
                     if ($cookieStore.get('token')) {
                         config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
                     }
                     return config;
                 },

                 // Intercept 401s and redirect you to login
                 responseError: function(response) {
                     if (response.status === 401) {
                         $location.path('/login');
                         // remove any stale tokens
                         $cookieStore.remove('token');
                         return $q.reject(response);
                     } else {
                         return $q.reject(response);
                     }
                 }
             };
         }])
         .run(['$rootScope', '$location', 'Auth', '$state', function($rootScope, $location, Auth, $state) {
             // Redirect to login if route requires auth and you're not logged in
             $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

                 Auth.isLoggedInAsync(function(loggedIn) {
                     if (toState.authenticate && !loggedIn) {
                         event.preventDefault();
                         $rootScope.returnToState = toState.url;
                         $rootScope.returnToStateParams = toParams.Id;
                         ///$location.path('/login');
                         $state.go('login');
                     }

                     if (loggedIn && toState.isAdmin && !Auth.isAdmin()) {
                         $state.go('settings');
                     }

                 });

             });
         }]);
 })();