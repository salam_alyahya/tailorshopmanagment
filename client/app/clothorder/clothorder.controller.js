(function() {
    'use strict';



    var clothOrderCtrl = function($scope, ClothOrder, clothorders) {
        $scope.onSave = updateForm;
        updateForm();

        function updateForm() {
            $scope.clothOrder = new ClothOrder();
            $scope.clothOrder.clothes = [{
                cloth: {},
                length: 0,
                unit: 'yards'
            }];
            $scope.clothOrder.date = new Date();
            $scope.clothorders = ClothOrder.query() || clothorders;
        }
    };

    clothOrderCtrl.$inject = ['$scope', 'ClothOrder', 'clothorders'];

    angular.module('tailorShopMngApp')
        .controller('ClothorderCtrl', clothOrderCtrl);
})();