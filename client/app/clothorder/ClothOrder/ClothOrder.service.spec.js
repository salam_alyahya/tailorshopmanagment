'use strict';

describe('Service: ClothOrder', function () {

  // load the service's module
  beforeEach(module('tailorShopMngApp'));

  // instantiate service
  var ClothOrder;
  beforeEach(inject(function (_ClothOrder_) {
    ClothOrder = _ClothOrder_;
  }));

  it('should do something', function () {
    expect(!!ClothOrder).toBe(true);
  });

});
