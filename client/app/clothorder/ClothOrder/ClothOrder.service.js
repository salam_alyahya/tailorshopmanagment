(function() {

    'use strict';

    angular.module('tailorShopMngApp')
        .factory('ClothOrder', ['$resource', function($resource) {
            // Service logic
            // ...

            var ClothOrder = $resource('/api/clothorders/:_id', {
                _id: '@_id'
            }, {
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        _id: '_id'
                    }
                },
                post: {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    transformRequest: function(data, header) {
                        return data;
                    },
                }
            });

            ClothOrder.prototype.newCloth = function() {
                this.clothes.push({
                    cloth: {},
                    length: 0,
                    unit: 'yards'
                });
            };
            // Public API here
            return ClothOrder;
        }]);
})();