 (function() {

     'use strict';

     function ClothorderRecord($scope, ClothOrder) {
         $scope.loadAll = loadAll;

         function loadAll() {
             $scope.clothorders = ClothOrder.query();
             //console.log($scope.clothorders);
         }
     }

     ClothorderRecord.$inject = ['$scope', 'ClothOrder'];

     angular.module('tailorShopMngApp')
         .directive('ngClothorderRecord', function() {
             return {
                 templateUrl: 'app/clothorder/ngClothorderRecord/ngClothorderRecord.html',
                 restrict: 'EA',
                 scope: {
                     clothorders: '='
                 },
                 link: function(scope, element, attrs) {
                     //scope.loadAll();
                 },
                 controller: ClothorderRecord
             };
         });


 })();
