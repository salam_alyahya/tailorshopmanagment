'use strict';

describe('Directive: ngClothorderRecord', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/clothorder/ngClothorderRecord/ngClothorderRecord.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-clothorder-record></ng-clothorder-record>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngClothorderRecord directive');
  }));
});
