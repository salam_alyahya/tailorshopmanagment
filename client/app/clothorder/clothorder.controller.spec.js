'use strict';

describe('Controller: ClothorderCtrl', function () {

  // load the controller's module
  beforeEach(module('tailorShopMngApp'));

  var ClothorderCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClothorderCtrl = $controller('ClothorderCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
