(function() {

    'use strict';

    var clothOrderFormCtrl = function($scope, Cloth, $log, Auth, $mdToast) {

        $scope.queryClothes = queryClothes;
        $scope.querySearch = querySearch;
        $scope.selectedItemChange = selectedItemChange;
        $scope.searchTextChange = searchTextChange;
        $scope.calcPrice = calcPrice;
        $scope.lengthOrderChange = lengthOrderChange;
        $scope.addCloth = addCloth;
        $scope.username = Auth.getCurrentUser().name;
        $scope.payAll = payAll;
        $scope.onSubmit = onSubmit;

        function queryClothes() {
            $scope.clothes = Cloth.query();
        }

        function onSubmit(ngModel, form) {
            //validateFrom($scope.clothOrderForm);
            form.refNo.$setTouched();
            if (form.$valid) {
                var cSanitized = sanitizeClothes(ngModel.clothes);
                ngModel.clothes = fixClothesForSubmit(cSanitized);
                ngModel.price = $scope.total; // set the total price
                ngModel.balance = $scope.total - ngModel.paid; // calculate the balance
                ngModel.user = Auth.getCurrentUser()._id; // set submitting user
                ngModel.$save(success, error);
            }

            function success(res) {
                //$log.info(res);
                showSuccessSubmitToast();
                $scope.total = 0;
                $scope.onSave();
                $scope.clothOrderForm.$setPristine();
                $scope.clothOrderForm.$setUntouched();
            }

            function error(err) {
                $log.error(err);
                showFailToast();
            }
        }

        // remove enteries that don't have an _id
        function sanitizeClothes(clothes) {
            return clothes.filter(function(cloth) {
                return angular.isDefined(cloth.cloth._id);
            });
        }

        function fixClothesForSubmit(clothes) {
            return clothes.map(function(cloth) {
                var id = cloth.cloth._id;
                cloth.cloth = id;
                return cloth;
            });
        }

        function addCloth() {
            $scope.ngModel.newCloth();
        }

        function searchTextChange(text) {
            //$log.info('search text changed: ' + text);
        }

        function selectedItemChange(item, cloth) {
            $log.info('selected item: ' + JSON.stringify(item));
            cloth.cloth = item;
        }

        function querySearch(query) {
            var results = query ? $scope.clothes.filter(createFilterFor(query)) : $scope.clothes;
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(cloth) {
                var name = angular.lowercase(cloth.name),
                    type = angular.lowercase(cloth.type);
                return (name.indexOf(lowercaseQuery) > -1) || (type.indexOf(lowercaseQuery) > -1);
            };
        }

        function calcPrice(clothItem, length) {
            clothItem.calculatedPrice = clothItem.cloth ? clothItem.cloth.unitSellPrice * length : 0;
        }

        function lengthOrderChange(cloth, length) {
            calcPrice(cloth, length);
            calcTotal();
        }

        function payAll() {
            $scope.ngModel.paid = $scope.total;
        }

        function calcTotal() {
            var total = 0;
            angular.forEach($scope.ngModel.clothes, function(cloth) {
                total += cloth.calculatedPrice;
            });
            $scope.total = total;
        }

        function showSuccessSubmitToast() {
            showToast('تم ادخال الفاتورة بنجاح');
        }

        function showFailToast() {
            showToast('اووبس، السستم تعبان وصار شي غلط');
        }

        function showToast(msg) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(msg)
                .position('bottom right')
                .hideDelay(3000)
            );
        }

        function validateFrom(form) {
            angular.forEach($scope.clothOrderForm.$error.required, function(field) {
                field.$setDirty();
            });
        }
    };

    clothOrderFormCtrl.$inject = ['$scope', 'Cloth', '$log', 'Auth', '$mdToast'];

    angular.module('tailorShopMngApp')
        .directive('ngClothOrderForm', function() {
            return {
                templateUrl: 'app/clothorder/ngClothOrderForm/ngClothOrderForm.html',
                restrict: 'EA',
                require: '?^ngModel',
                scope: {
                    ngModel: '=',
                    onSave: '&',
                    ngSelected: '='
                },
                controller: clothOrderFormCtrl,
                link: function(scope, element, attrs) {

                    scope.$watch('ngSelected', function(old, _new) {
                        if (old)
                            scope.queryClothes();
                    });
                }
            };
        });

})();
