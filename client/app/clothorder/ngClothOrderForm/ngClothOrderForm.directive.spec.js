'use strict';

describe('Directive: ngClothOrderForm', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/clothorder/ngClothOrderForm/ngClothOrderForm.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-cloth-order-form></ng-cloth-order-form>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngClothOrderForm directive');
  }));
});
