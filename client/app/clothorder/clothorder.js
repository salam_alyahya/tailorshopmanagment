 (function() {
     'use strict';
     var getClothOrders = function(ClothOrder) {
         return ClothOrder.query();
     };
     getClothOrders.$inject = ['ClothOrder'];

     angular.module('tailorShopMngApp')
         .config(['$stateProvider', function($stateProvider) {
             $stateProvider
                 .state('clothorder', {
                     url: '/clothorder',
                     templateUrl: 'app/clothorder/clothorder.html',
                     controller: 'ClothorderCtrl',
                     resolve: {
                         'clothorders': getClothOrders
                     },
                     authenticate: true
                 });
         }]);


 })();