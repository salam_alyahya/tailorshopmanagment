 (function() {

     'use strict';

     var AdminCtrl = function($scope, $http, Auth, User, $mdDialog) {

         // Use the User $resource to fetch all users
         $scope.users = [];
         $scope.onEdit = onEdit;
         $scope.delete = _delete;
         update();

         function _delete(user) {
             User.remove({
                 id: user._id
             });
             angular.forEach($scope.users, function(u, i) {
                 if (u === user) {
                     $scope.users.splice(i, 1);
                 }
             });
         }

         function onEdit(ev, user) {
             showDialog(ev, user);
         }

         function update() {
             $scope.users = User.query();
         }

         function showDialog(ev, user) {
             $mdDialog.show({
                     controller: EditDialogController,
                     templateUrl: 'app/admin/editAdminDialog.tmpl.html',
                     parent: angular.element('#content'),
                     targetEvent: ev,
                     clickOutsideToClose: true,
                     fullscreen: false,
                     locals: {
                         user: user
                     }
                 })
                 .then(function confirm(user) {
                     User.update(user, function(res) {
                         update();
                     });
                 }, function cancel() {
                     //dialog cancelled
                 });
         }
     };
     AdminCtrl.$inject = ['$scope', '$http', 'Auth', 'User', '$mdDialog'];

     var EditDialogController = function($scope, $mdDialog, user) {
         $scope.user = angular.fromJson(angular.toJson(user));
         $scope.userRoles = ['admin', 'user'];
         $scope.hide = function() {
             $mdDialog.hide();
         };
         $scope.cancel = function() {
             $mdDialog.cancel();
         };
         $scope.answer = function(form, user) {
             if (form.$valid) {
                 $mdDialog.hide(user);
             }
         };
     };
     EditDialogController.$inject = ['$scope', '$mdDialog', 'user'];
     angular.module('tailorShopMngApp')
         .controller('AdminCtrl', AdminCtrl);
 })();
