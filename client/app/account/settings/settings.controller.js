(function() {

    'use strict';



    var settingsCtrl = function($scope, User, Auth, $mdToast) {
        $scope.errors = {};
        $scope.changePassword = changePassword;

        function showSuccessToast() {
            showToast('تم تغيير كلمة المرور');
        }

        function showToast(msg) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(msg)
                .position('bottom right')
                .hideDelay(3000)
            );
        }

        function changePassword(form) {
            $scope.submitted = true;
            if (form.$valid) {
                Auth.changePassword($scope.user.oldPassword, $scope.user.newPassword)
                    .then(sucess)
                    .catch(error);
            }

            function sucess() {
                showSuccessToast();
            }

            function error(err) {
                form.password.$setValidity('mongoose', false);
                $scope.errors.other = 'Incorrect password';
                $scope.message = '';
                showToast($scope.errors.other);
            }
        }
    }
    settingsCtrl.$inject = ['$scope', 'User', 'Auth', '$mdToast'];
    angular.module('tailorShopMngApp')
        .controller('SettingsCtrl', settingsCtrl);

})();