(function() {

    'use strict';



    var signupController = function($scope, Auth, $location) {
        $scope.user = {};
        $scope.errors = {};

        $scope.register = function(form) {
            $scope.submitted = true;

            if (form.$valid) {
                Auth.createUser({
                        name: $scope.user.name,
                        username: $scope.user.username,
                        password: $scope.user.password
                    })
                    .then(function() {
                        // Account created, redirect to home
                        $location.path('/');
                    })
                    .catch(function(err) {
                        err = err.data;
                        $scope.errors = {};

                        // Update validity of form fields that match the mongoose errors
                        angular.forEach(err.errors, function(error, field) {
                            form[field].$setValidity('mongoose', false);
                            $scope.errors[field] = error.message;
                        });
                    });
            }
        };

    }
    signupController.$inject = ['$scope', 'Auth', '$location'];
    angular.module('tailorShopMngApp')
        .controller('SignupCtrl', signupController);

})();