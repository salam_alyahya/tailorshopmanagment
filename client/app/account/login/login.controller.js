(function() {
    'use strict';

    var loginController = function($scope, Auth, $location, $rootScope) {
        $scope.user = {};
        $scope.errors = {};

        $scope.login = function(form) {
            $scope.submitted = true;

            if (form.$valid) {
                Auth.login({
                        username: $scope.user.username,
                        password: $scope.user.password
                    })
                    .then(function() {
                        // Logged in, redirect to home
                        if ($rootScope.returnToState) {
                            $location.path($rootScope.returnToState);
                        }
                        $location.path('/');
                    })
                    .catch(function(err) {
                        $scope.errors.other = err.message;
                        Auth.parseForError(err, form);
                    });
            }
        };

        $scope.onChange = function(form) {
            form.username.$setValidity('registered', true);
            form.password.$setValidity('correct', true);
            form.$setPristine();
            form.$setUntouched();
        };
    }
    loginController.$inject = ['$scope', 'Auth', '$location', '$rootScope'];

    angular.module('tailorShopMngApp')
        .controller('LoginCtrl', loginController);
})();