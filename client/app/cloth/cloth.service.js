(function() {

    'use strict';

    angular.module('tailorShopMngApp')
        .factory('Cloth', ['$resource', function($resource) {
            // Service logic
            // ...

            var Cloth = $resource('/api/cloths/:_id', {
                _id: '@_id'
            }, {
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    params: {
                        _id: '_id'
                    }
                }
            });

            Cloth.prototype.addPurchase = function(purchase) {
                if (this.rollsPurchase) {
                    this.rollsPurchase.push(purchase);
                } else {
                    this.rollsPurchase = [purchase];
                }

                if (angular.isUndefined(this.totalLength)) {
                    this.totalLength = 0;
                }

                for (var i = purchase.purchaseCount - 1; i >= 0; i--) {
                    this.totalLength += purchase.unitLength;
                }

                this.rollsCount += purchase.purchaseCount;
            };

            // Public API here
            return Cloth;
        }]);
})();