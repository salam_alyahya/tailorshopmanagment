(function() {

    'use strict';

    var ClothListItemCtrl = function($scope, $mdDialog, Cloth) {
        $scope.showDialog = showDialog;

        function showDialog(ev) {

            $mdDialog.show({
                controller: ['$scope', '$mdDialog', '_cloth', 'model', PurchaseDialogController],
                templateUrl: 'app/cloth/ngClothListItem/ngClothPurchaseDialog.tmpl.html',
                parent: angular.element('#content'),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false,
                locals: {
                    _cloth: $scope.cloth,
                    model: $scope.model
                }
            }).then(confirm, cancel);

            function cancel() {
                //dialog cancelled
            }

            function confirm(arg) {
                var model = arg.model,
                    cloth = arg.cloth;

                var _c = new Cloth(cloth);
                model.availableCount = _c.rollsCount;
                _c.addPurchase(model);
                _c.$update({
                    '_id': cloth._id
                }, function success(_model) {
                    //$scope.onEdit(_model);
                    $scope.onUpdate();
                });
            }
        }
    };
    ClothListItemCtrl.$inject = ['$scope', '$mdDialog', 'Cloth'];


    var PurchaseDialogController = function($scope, $mdDialog, _cloth, model) {
            $scope.model = model;
            $scope.ngModel = angular.fromJson(angular.toJson(_cloth));
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(form, model) {
                if (form.$valid) {
                    $mdDialog.hide({ 'model': $scope.model, 'cloth': $scope.ngModel });
                }
            };
        }
        //PurchaseDialogController.$inject = ['$scope', '$mdDialog', '_cloth', 'model'];


    angular.module('tailorShopMngApp')
        .directive('ngClothListItem', function() {
            return {
                templateUrl: 'app/cloth/ngClothListItem/ngClothListItem.html',
                restrict: 'EA',
                scope: {
                    cloth: '=',
                    onEdit: '&',
                    ngSelected: '=',
                    onRemove: '&',
                    unit: '=',
                    showEdit: '=',
                    onUpdate: '&'
                },
                controller: ClothListItemCtrl,
                link: function(scope, element, attrs) {
                    scope.model = {
                        dateOfPurchase: new Date(),
                    };

                    scope.$watch('cloth.selected', function(selected) {
                        if (!selected) {
                            scope.cloth.showPurchase = false;
                        }
                    });
                }
            };
        });
})();
