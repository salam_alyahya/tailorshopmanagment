'use strict';

describe('Service: Cloth', function () {

  // load the service's module
  beforeEach(module('tailorShopMngApp'));

  // instantiate service
  var Cloth;
  beforeEach(inject(function (_Cloth_) {
    Cloth = _Cloth_;
  }));

  it('should do something', function () {
    expect(!!Cloth).toBe(true);
  });

});
