'use strict';

describe('Directive: ngClothPurchaseForm', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/cloth/ngClothPurchaseForm/ngClothPurchaseForm.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-cloth-purchase-form></ng-cloth-purchase-form>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngClothPurchaseForm directive');
  }));
});
