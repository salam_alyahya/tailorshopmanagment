(function() {

    'use strict';

    var ClothPurchaseFormContoller = function($scope, $mdDialog) {
        $scope.onSave = onSave;

        function onSave(ev, model) {
            model.availableCount = model.purchaseCount;
            $scope.ngModel.rollsPurchase = [model];
            $scope.ngModel.rollsCount += model.purchaseCount;
            $scope.ngModel.$update({
                '_id': $scope.ngModel._id
            }, function(_model) {
                //console.log(_model);
            });
        }
    };
    ClothPurchaseFormContoller.$inject = ['$scope', '$mdDialog'];

    angular.module('tailorShopMngApp')
        .directive('ngClothPurchaseForm', function() {
            return {
                templateUrl: 'app/cloth/ngClothPurchaseForm/ngClothPurchaseForm.html',
                restrict: 'EA',
                require: '?^ngModel',
                scope: {
                    ngModel: '=',
                    onBack: '&'
                },
                controller: ClothPurchaseFormContoller,
                link: function(scope, element, attrs) {
                    //console.log(scope.ngModel);
                    scope.model = {
                        dateOfPurchase: new Date(),
                    };
                }
            };
        });
})();