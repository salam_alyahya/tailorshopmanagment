 (function() {
     'use strict';

     var getClothes = function(Cloth) {
         return Cloth.query();
     }
     getClothes.$inject = ['Cloth'];

     angular.module('tailorShopMngApp')
         .config(['$stateProvider', function($stateProvider) {
             $stateProvider
                 .state('cloth', {
                     url: '/cloth',
                     templateUrl: 'app/cloth/cloth.html',
                     controller: 'ClothCtrl',
                     resolve: {
                         'clothes': getClothes
                     },
                     authenticate: true
                 });
         }]);


 })();