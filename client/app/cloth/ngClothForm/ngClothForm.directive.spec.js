'use strict';

describe('Directive: ngClothForm', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('app/cloth/ngClothForm/ngClothForm.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-cloth-form></ng-cloth-form>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngClothForm directive');
  }));
});
