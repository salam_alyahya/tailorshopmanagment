  (function() {
      'use strict';



      function clothFormLink(scope, element, attrs) {
          scope.units = [
              'Inches',
              'Meters',
              'Centimeters'
          ];
          //scope.queryDealers();

          scope.$watch('ngSelected', function(old, _new){
            console.log(old);
            if(old)
              scope.queryDealers();
          });
      }

      var ClothFormController = function($scope, Dealer, $log, Cloth, $mdToast) {
          $scope.queryDealers = queryDealers;
          $scope.querySearch = querySearch;
          $scope.selectedItemChange = selectedItemChange;
          $scope.searchTextChange = searchTextChange;
          $scope.submitForm = submitForm;
          $scope.checkModel = updateModel;
          $scope.calcTotalLength = calcTotalLength;

          function queryDealers() {
              $scope.dealers = Dealer.query();
          }

          function updateModel() {
              if ($scope.ngModel && $scope.ngModel._id) {
                  $scope.ngModel = createNewCloth();
              }
          }

          function createNewCloth() {
              return new Cloth({
                  'rollsPurchase': [{
                      'dateOfPurchase': new Date(),
                  }]
              });
          }

          function searchTextChange(text) {
              $log.info('search text changed: ' + text);
          }

          function selectedItemChange(item) {
              if (item) {
                  $scope.ngModel.dealer = item._id;
              }
          }

          function querySearch(query) {
              var results = query ? $scope.dealers.filter(createFilterFor(query)) : $scope.dealers;
              return results;
          }

          function createFilterFor(query) {
              var lowercaseQuery = angular.lowercase(query);
              return function filterFn(dealer) {
                  return (dealer.name.indexOf(lowercaseQuery) > -1);
              };
          }

          function submitForm(cloth, form) {
              $log.info('Save');
              //validateFrom();
              form.name.$setTouched();
              if (form.$valid) {
                  var purchase = cloth.rollsPurchase[0];
                  //loth.rollsPurchase = [];
                  if (cloth._id) {
                      cloth.$update(done, err);
                  } else {
                      purchase = cloth.rollsPurchase[0];
                      //cloth.rollsPurchase = [];
                      purchase.unitLength = cloth.unitLength;
                      //cloth.addPurchase(purchase);
                      //cloth.totalLength = calcTotalLength(cloth.rollsPurchase);
                      cloth.$save(done, err);
                  }
              } else {
                //showFailToast();

              }

              function done(res) {
                  $scope.onSave(res);
                  //$scope.ngModel = createNewCloth();
                  $scope.searchText = '';
                  $scope.selectedItem = undefined;
                  $scope.clothForm.$setPristine();
                  $scope.clothForm.$setUntouched();
                  $scope.onUpdate();
                  showSuccessToast();
              }

              function err(err) {
                  $log.error(err);
                  showFailToast();
              }
          }

          function calcTotalLength(rollsPurchase) {
              var count = 0;
              rollsPurchase.forEach(function(purchase) {
                  count += purchase.purchaseCount;
                  purchase.availableCount = purchase.purchaseCount;
              });
              $scope.ngModel.rollsCount = count;
              $scope.ngModel.totalLength = $scope.ngModel.rollsCount * $scope.ngModel.unitLength;
              return $scope.ngModel.totalLength;
          }

          function showSuccessToast() {
              showToast('تم ادخال القماش بنجاح');
          }

          function showFailToast() {
              showToast('اووبس، السستم تعبان وصار شي غلط');
          }

          function showToast(msg) {
              $mdToast.show(
                  $mdToast.simple()
                  .textContent(msg)
                  .position('bottom right')
                  .hideDelay(3000)
              );
          }

          
          
      };


      ClothFormController.$inject = ['$scope', 'Dealer', '$log', 'Cloth', '$mdToast'];

      angular.module('tailorShopMngApp')
          .directive('ngClothForm', function() {
              return {
                  templateUrl: 'app/cloth/ngClothForm/ngClothForm.html',
                  restrict: 'EA',
                  scope: {
                      ngModel: '=',
                      onSave: '&',
                      ngSelected: '=',
                      onUpdate: '&'
                  },
                  controller: ClothFormController,
                  link: clothFormLink
              };
          });
  })();
