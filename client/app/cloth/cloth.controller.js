(function() {

    'use strict';

    var clothController = function($scope, Cloth, $log, $mdDialog, Auth, clothes, $mdToast) {
        $scope.cloths = [];
        $scope.update = update;
        $scope.removeCloth = removeCloth;
        $scope.editCloth = editCloth;
        $scope.selectedIndex = 1;
        $scope.selectItem = selectItem;
        $scope.selectedItem = {};
        $scope.isAdmin = Auth.isAdmin;
        $scope.cloths = clothes;
        //update();

        $scope.$watch('selectedIndex', function(current, old) {
            if (current === 1) {
                resetClothModel();
            }
        });


        function update() {
            $scope.cloths = Cloth.query();

            resetClothModel();
        }

        function resetClothModel() {
            $scope.editClothModel = new Cloth({
                'rollsPurchase': [{
                    'dateOfPurchase': new Date(),
                }]
            });
        }

        function editCloth(cloth) {
            $scope.editClothModel = cloth;
            $scope.selectedIndex = 0;
        }

        function removeCloth($event, cloth, $index) {
            showConfirm($event, sucess, error);

            function sucess() {
                cloth.$remove(function(res) {
                    $log.info('remove');
                    //update();
                    $scope.cloths.splice($index, 1);
                    showSuccessDeleteToast();
                }, error);
            }

            function error(error) {
                $log.info('pls do not delete this peice of cloth now', error);
                if (error) {
                    showFailToast();
                }
            }
        }

        function showConfirm(ev, sucess, err) {
            //Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('هل انت متاكد؟')
                .textContent('سيتم حذف هذه المعلومات')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('اي احذفه له!')
                .cancel('لا لا وش لك');
            $mdDialog.show(confirm).then(sucess, err);
        }

        function selectItem(cloth) {
            if (cloth !== $scope.selectedItem) {
                $scope.selectedItem.selected = false;
                cloth.selected = true;
                $scope.selectedItem = cloth;
            } else {
                cloth.selected = false;
                $scope.selectedItem = {};
            }
        }

        function showSuccessDeleteToast() {
            showToast('تم حذف القماش بنجاح');
        }

        function showFailToast() {
            showToast('اووبس، السستم تعبان وصار شي غلط');
        }

        function showToast(msg) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(msg)
                .position('bottom right')
                .hideDelay(3000)
            );
        }
    }

    clothController.$inject = ['$scope', 'Cloth', '$log', '$mdDialog', 'Auth', 'clothes', '$mdToast'];

    angular.module('tailorShopMngApp')
        .controller('ClothCtrl', clothController);
})();