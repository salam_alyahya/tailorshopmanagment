'use strict';

angular.module('tailorShopMngApp')
    .directive('fullHeight', ['$window', function($window) {
        return {
            restrict: 'EA',
            link: function(scope, element, attrs) {
                var w = angular.element('#content');
                scope.getWindowDimensions = function() {
                    return {
                        'h': w.height(),
                        'w': w.width()
                    };
                };

                scope.$watch(scope.getWindowDimensions, function(newValue, oldValue) {
                    console.log('%s, %s', scope.getWindowDimensions().h, scope.getWindowDimensions().w);

                    element.css('max-height', scope.getWindowDimensions().h - 174);
                }, true);
            }
        };
    }]);