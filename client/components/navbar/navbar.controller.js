(function() {
    'use strict';



    var navbarController = function($scope, $location, Auth, $mdSidenav, $mdBottomSheet, $q, $state, $timeout, $log) {
        $scope.menu = [{
            'title': 'Home',
            'link': 'main',
            'icon': 'ic_home_black_48px.svg',
            'active': true,
            'isRole': function() {
                return true;
            }
        }, {
            'title': 'المستخدمين',
            'link': 'admin',
            'icon': 'account_box_24px.svg',
            'active': false,
            'isRole': Auth.isAdmin
        }, {
            'title': 'الموزع',
            'link': 'dealer',
            'icon': 'assignment_returned_24px.svg',
            'active': false,
            'isRole': Auth.isLoggedIn
        }, {
            'title': 'القماش',
            'link': 'cloth',
            'icon': 'store_24px.svg',
            'active': false,
            'isRole': Auth.isLoggedIn
        }, {
            'title': 'بيع القماش',
            'link': 'clothorder',
            'icon': 'work_24px.svg',
            'active': false,
            'isRole': Auth.isLoggedIn
        }];

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.getCurrentUser = Auth.getCurrentUser;
        $scope.toggleSidenav = toggleSidenav;
        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.selectedMenuItem = $scope.menu[0];

        //$state.go($location.path().split('/')[1]);

        $scope.isOpenRight = function() {
            return $mdSidenav('right').isOpen();
        };

        $scope.logout = function() {
            Auth.logout();
            $location.path('/login');
        };

        $scope.navigate = function(path) {
            console.log(path);
            //$location.path(path);
            $state.transitionTo(path);
        };

        $scope.isActive = function(route) {
            return route === $location.path();
        };

        function toggleSidenav(name) {
            $mdSidenav(name).toggle();
        }

        if ($scope.menu[0]) {
            $scope.currentTabTitle = $scope.menu[0].title;
        }

        $scope.selectTab = function(item) {
            $scope.toggleSideNav();
            $scope.currentTabTitle = item.title;
            $scope.selectedMenuItem.active = false;
            $scope.selectedMenuItem = item;
            item.active = true;
            console.log(item);
            //$location.path(item.link);
            $state.transitionTo(item.link);
        };


        $scope.toggleSideNav = function() {
            var pending = $mdBottomSheet.hide() || $q.when(true);
            pending.then(function() {
                $mdSidenav('right').toggle();
            });
        };

        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {
            return debounce(function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function() {
                        $log.debug('toggle ' + navID + ' is done');
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function() {
                        $log.debug('toggle ' + navID + ' is done');
                    });
            };
        }
        //$location.path($scope.menu[0].link);
        //$state.transitionTo($scope.menu[0].link);
        $scope.toggleRight();


    };
    navbarController.$inject = ['$scope', '$location', 'Auth', '$mdSidenav', '$mdBottomSheet', '$q', '$state', '$timeout', '$log'];

    angular.module('tailorShopMngApp')
        .controller('NavbarCtrl', navbarController);

})();