'use strict';

angular.module('tailorShopMngApp')
    .filter('unitConverterFilter', function() {
        var units = {
            // Size/distance
            'mm': 0.001,
            'cm': 0.01,
            'm': 1,
            'in': 0.0254,
            'ft': 0.3048,
            'yd': 0.9144,
        };
        return function(input, convertFrom, convertTo) {
            if (input) {
                var from = new Decimal(units[convertFrom]);
                var to = new Decimal(units[convertTo]);
                return (new Decimal(input).times(from.dividedBy(to))).toNumber();
            }
            return input;
        };
    });