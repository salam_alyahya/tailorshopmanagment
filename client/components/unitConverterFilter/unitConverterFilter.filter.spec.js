'use strict';

describe('Filter: unitConverterFilter', function () {

  // load the filter's module
  beforeEach(module('tailorShopMngApp'));

  // initialize a new instance of the filter before each test
  var unitConverterFilter;
  beforeEach(inject(function ($filter) {
    unitConverterFilter = $filter('unitConverterFilter');
  }));

  it('should return the same input since units are the same (m to m)', function () {
    var input = 100,
        convertFrom = 'm',
        convertTo = 'm';
    expect(unitConverterFilter(input, convertFrom, convertTo)).toBe(input);
  });

  it('should return the right converted value (m to cm)', function () {
    var input = 100,
        convertFrom = 'm',
        convertTo = 'cm',
        output = input*100;
    expect(unitConverterFilter(input, convertFrom, convertTo)).toBe(output);
  });

});
