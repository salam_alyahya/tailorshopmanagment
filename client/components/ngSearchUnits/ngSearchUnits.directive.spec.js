'use strict';

describe('Directive: ngSearchUnits', function () {

  // load the directive's module and view
  beforeEach(module('tailorShopMngApp'));
  beforeEach(module('components/ngSearchUnits/ngSearchUnits.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-search-units></ng-search-units>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngSearchUnits directive');
  }));
});
