'use strict';

angular.module('tailorShopMngApp')
    .directive('ngSearchUnits', function() {
        return {
            templateUrl: 'components/ngSearchUnits/ngSearchUnits.html',
            restrict: 'EA',
            scope: {
                'unit': '=',
                'searchValue': '='
            },
            link: function(scope, element, attrs) {
                console.log('im here');
            }
        };
    });